import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NgRxStoreModule } from './store/store.module';
import { AppComponent } from './app.component';
import { ClientsComponent } from './clients/clients.component';

@NgModule({
    imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        HttpClientModule,
        NgbModule,
        NgRxStoreModule
    ],
    declarations: [
        AppComponent,
        ClientsComponent
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
