import { Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EntityCollectionService, EntityCollectionServiceFactory } from '@ngrx/data';
import { Subscription } from 'rxjs';

export class Client {
    id!: number;
    name: string = '';
    email: string = '';
    phone: string = '';
    providers: Provider[] = [];
}

export class Provider {
    id!: number;
    name: string = '';
}

@Component({
    selector: 'app-clients',
    templateUrl: './clients.component.html',
    styleUrls: ['./clients.component.scss'],
    styles: [`
        input.ng-touched.ng-invalid {border:solid red 2px;}
        input.ng-touched.ng-valid {border:solid green 2px;}
    `]
})
export class ClientsComponent implements OnInit, OnDestroy {

    clients: Client[] = [];
    providers: Provider[] = [];
    subscriptions: Subscription[] = [];
    currentClient = new Client();
    currentProvider = new Provider();
    editMode = false;
    providersEditMode = false;
    clientService: EntityCollectionService<Client>;
    providerService: EntityCollectionService<Provider>;

    constructor(
        EntityCollectionServiceFactory: EntityCollectionServiceFactory,
        private modalService: NgbModal
    ) {
        this.clientService = EntityCollectionServiceFactory.create<Client>('Client')
        this.providerService = EntityCollectionServiceFactory.create<Provider>('Provider')
    }

    ngOnInit(): void {
        this.subscriptions.push(this.clientService.getAll().subscribe(res => this.clients = res));
        this.subscriptions.push(this.providerService.getAll().subscribe(res => this.providers = res));
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subs => subs.unsubscribe());
    }

    join(providers: Provider[]): string {
        return providers.length ? providers.map(item => item.name).join(', ') : '';
    }

    add(content: TemplateRef<any>) {
        this.editMode = false;
        this.currentClient = new Client();
        this.modalService.open(content, { size: 'lg' }).result.then((result) => {
            this.clientService.add(result).subscribe(res => this.clients.push(res));
        }, () => {});
    }

    edit(content: TemplateRef<any>, client: Client) {
        this.editMode = true;
        this.currentClient = {...client};
        this.modalService.open(content, { size: 'lg' }).result.then((result) => {
            this.clientService.update(result).subscribe(res => {
                let idx = this.clients.findIndex(item => item.id == res.id);
                this.clients.splice(idx, 1, res);
            });
        }, () => {});
    }

    delete(clientId: number) {
        this.clientService.delete(clientId).subscribe(() => {
            this.clients = this.clients.filter(item => item.id != clientId);
        });
    }

    trackByFn(index: number, item: Client | Provider): number {
        return item.id;
    }

    submitProvider() {
        if (this.providersEditMode) {
            this.providerService.update(this.currentProvider).subscribe(res => {
                let idx = this.providers.findIndex(item => item.id == this.currentProvider.id);
                this.providers.splice(idx, 1, res);
                this.providersEditMode = false;
                this.currentProvider = new Provider();
            });
        } else {
            this.providerService.add(this.currentProvider).subscribe(res => {
                this.providers.push(res);
                this.currentProvider = new Provider();
            });
        }
    }

    editProvider(provider: Provider) {
        this.providersEditMode = true;
        this.currentProvider = {...provider};
    }

    deleteProvider(providerId: number) {
        this.providerService.delete(providerId).subscribe(() => {
            this.providers = this.providers.filter(item => item.id != providerId);
        })
    }

    isChecked(provider: Provider): boolean {
        return this.currentClient.providers.map(item => item.id).includes(provider.id);
    }

    onCheck(e: any, provider: Provider) {
        if (e.target.checked) {
            const arr = [...this.currentClient.providers];
            arr.push(provider);
            this.currentClient.providers = arr;
        } else {
            this.currentClient.providers = this.currentClient.providers.filter(item => item.id != provider.id);
        }
    }
}
