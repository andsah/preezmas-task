import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Provider } from '../clients/clients.component';

@Injectable({
    providedIn: 'root'
})
export class ProviderService extends EntityCollectionServiceBase<Provider>{

    constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
        super('Provider', serviceElementsFactory)
    }
}
