import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Client } from '../clients/clients.component';

@Injectable({
    providedIn: 'root'
})
export class ClientsService extends EntityCollectionServiceBase<Client>{

    constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
        super('Client', serviceElementsFactory)
    }
}
