import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { DefaultDataServiceConfig, EntityDataModule, HttpUrlGenerator } from '@ngrx/data';

import { entityConfig } from './entity-metadata';
import { PluralHttpUrlGenerator } from './plural-http-url-generator';

const defaultDataServiceConfig: DefaultDataServiceConfig = {
    root: 'http://localhost:3000',
    timeout: 3000
}

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        EntityDataModule.forRoot(entityConfig)
    ],
    providers: [
        { provide: DefaultDataServiceConfig, useValue: defaultDataServiceConfig },
        { provide: HttpUrlGenerator, useClass: PluralHttpUrlGenerator }
    ]
})
export class NgRxStoreModule { }
