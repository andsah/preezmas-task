import { EntityMetadataMap } from '@ngrx/data';

const entityMetadata: EntityMetadataMap = {
    Client: {},
    Provider: {}
};

const pluralNames = {};

export const entityConfig = {
    entityMetadata,
    pluralNames
};